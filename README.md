# Sophie's Skooma Sweetshoppe

Words go here!

#### Credits

Author: **Sophie**

#### Web

[Project Home](https://modding-openmw.gitlab.io/sophies-skooma-sweetshoppe/)

<!-- [Nexus Mods](https://www.nexusmods.com/morrowind/mods/#TODO) -->

[Source on GitLab](https://gitlab.com/modding-openmw/sophies-skooma-sweetshoppe)

#### Installation

1. Download the mod from [this URL](https://modding-openmw.gitlab.io/sophies-skooma-sweetshoppe/)
1. Extract the zip to a location of your choosing, examples below:

        # Windows
        C:\games\OpenMWMods\Gameplay\sophies-skooma-sweetshoppe

        # Linux
        /home/username/games/OpenMWMods/Gameplay/sophies-skooma-sweetshoppe

        # macOS
        /Users/username/games/OpenMWMods/Gameplay/sophies-skooma-sweetshoppe

#### Report A Problem

If you've found an issue with this mod, or if you simply have a question, please use one of the following ways to reach out:

* [Open an issue on GitLab](https://gitlab.com/modding-openmw/sophies-skooma-sweetshoppe/-/issues)
* Contact the author on Discord: `@Sophie<3~`
