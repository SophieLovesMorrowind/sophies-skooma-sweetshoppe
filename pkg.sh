#!/bin/sh
set -e

modname=sophies-skooma-sweetshoppe
file_name=$modname.zip

cat > version.txt <<EOF
Mod version: $(git describe --tags)
EOF

zip --must-match --recurse-paths ${file_name} \
    "00 Imperial Lighthouse Revamped" \
    CHANGELOG.md \
    LICENSE \
    README.md \
    version.txt

sha256sum ${file_name} > ${file_name}.sha256sum.txt
sha512sum ${file_name} > ${file_name}.sha512sum.txt
